# kotlin-microservices

## Currency Converter Service
port = `localhost:8100`
```
@GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")


from - currency to be converted
to - currency where it will be converted
quantiy - amount to be converted
```

## Currency Converter Service w/ feign
```
@GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
same as above
```

---

## Currency Exchange Service
port = `localhost:8000`
```
@GetMapping("/currency-exchange/from/{from}/to/{to}")


from - currency to be converted
to - currency where it will be converted

```

---

## default data's for conversion 
```
INSERT INTO exchange_value(id, currency_From, currency_To, conversion_Multiple, port)
VALUES
    (100001, 'USD', 'INR', 65, 0),
    (100002, 'EUR', 'INR', 75, 0),
    (100003, 'AUD', 'INR', 75, 0);
```