package com.microservices.limitsmicroservices

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class LimitsMicroservicesApplication

fun main(args: Array<String>) {
	runApplication<LimitsMicroservicesApplication>(*args)
}
