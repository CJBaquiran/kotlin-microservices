package com.microservices.limitsmicroservices.controller

import com.microservices.limitsmicroservices.component.Configuration
import com.microservices.limitsmicroservices.model.LimitConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class LimitsConfigurationController(@Autowired var configuration: Configuration) {
    @GetMapping("/limits")
    fun retrieveLimitsFromConfigurations(): LimitConfiguration {
        return LimitConfiguration(configuration.maximum, configuration.minimum)
    }
}
