package com.microservices.limitsmicroservices.component

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import kotlin.properties.Delegates

@Component
@ConfigurationProperties("limits-service")
class Configuration (
    var minimum: Int = 0,
    var maximum: Int = 0
)