package com.microservices.limitsmicroservices.model

class LimitConfiguration(
        val maximum: Int,
        val minimum: Int
)