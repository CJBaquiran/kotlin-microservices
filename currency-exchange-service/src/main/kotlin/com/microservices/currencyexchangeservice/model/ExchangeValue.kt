package com.microservices.currencyexchangeservice.model

import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
class ExchangeValue {
    @Id
    var id: Long? = null

    @Column(name="currency_From")
    var from: String? = null

    @Column(name="currency_To")
    var to: String? = null

    @Column(name="conversion_Multiple")
    var conversionMultiple: BigDecimal? = null
    var port: Int = 0

    constructor() {

    }

    constructor(id: Long?, from: String, to: String, conversionMultiple: BigDecimal) : super() {
        this.id = id
        this.from = from
        this.to = to
        this.conversionMultiple = conversionMultiple
    }
}