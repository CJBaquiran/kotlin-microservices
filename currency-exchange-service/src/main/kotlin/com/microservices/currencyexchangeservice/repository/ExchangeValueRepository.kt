package com.microservices.currencyexchangeservice.repository

import com.microservices.currencyexchangeservice.model.ExchangeValue
import org.springframework.data.jpa.repository.JpaRepository

interface ExchangeValueRepository: JpaRepository<ExchangeValue, Long > {
    fun findByFromAndTo(from: String, to: String): ExchangeValue
}