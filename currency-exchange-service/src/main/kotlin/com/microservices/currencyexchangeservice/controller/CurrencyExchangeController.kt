package com.microservices.currencyexchangeservice.controller

import com.microservices.currencyexchangeservice.model.ExchangeValue
import com.microservices.currencyexchangeservice.repository.ExchangeValueRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class CurrencyExchangeController (@Autowired val environment: Environment,
                                  @Autowired val exchangeValueRepository: ExchangeValueRepository){
    @GetMapping("/currency-exchange/from/{from}/to/{to}")
    fun retrieveExchangeValue(@PathVariable from: String, @PathVariable to: String): ExchangeValue{
        val exchangeValue = exchangeValueRepository.findByFromAndTo(from,to)
        exchangeValue.port = (Integer.parseInt(environment.getProperty("local.server.port")))

        return exchangeValue
    }
}