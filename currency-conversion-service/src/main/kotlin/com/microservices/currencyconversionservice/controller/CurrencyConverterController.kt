package com.microservices.currencyconversionservice.controller

import com.microservices.currencyconversionservice.model.CurrencyConversion
import com.microservices.currencyconversionservice.proxy.CurrencyExchangeServiceProxy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.client.RestTemplate
import java.math.BigDecimal
import java.util.*

@RestController
class CurrencyConverterController (@Autowired val currencyExchangeServiceProxy: CurrencyExchangeServiceProxy){
    @GetMapping("/currency-converter/from/{from}/to/{to}/quantity/{quantity}")
    fun convertCurrency(@PathVariable from: String, @PathVariable to: String, @PathVariable quantity: BigDecimal): CurrencyConversion {
        val uriVariables = HashMap<String, String>()
        uriVariables["from"] = from
        uriVariables["to"] = to

        val responseEntity = RestTemplate().getForEntity(
                "http://localhost:8000/currency-exchange/from/{from}/to/{to}",
                CurrencyConversion::class.java,
                uriVariables)

        val response = responseEntity.body

        return CurrencyConversion(
            response!!.id,
            from,
            to,
            response.conversionMultiple,
            quantity,
            quantity.multiply(response.conversionMultiple!!),
            response.port)
    }

    @GetMapping("/currency-converter-feign/from/{from}/to/{to}/quantity/{quantity}")
    fun convertCurrencyFeign(@PathVariable from: String, @PathVariable to: String, @PathVariable quantity: BigDecimal): CurrencyConversion {

        val response = currencyExchangeServiceProxy.retrieveExchangeValue(from, to)

        return CurrencyConversion(
                response!!.id,
                from,
                to,
                response.conversionMultiple,
                quantity,
                quantity.multiply(response.conversionMultiple!!),
                response.port)
    }
}