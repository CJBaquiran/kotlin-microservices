package com.microservices.currencyconversionservice.model

import java.math.BigDecimal

class CurrencyConversion(
    var id: Long? = null,
    var from: String? = null,
    var to: String? = null,
    var conversionMultiple: BigDecimal? = null,
    var quantity: BigDecimal? = null,
    var totalCalculatedAmmount: BigDecimal? = null,
    var port: Int
    )